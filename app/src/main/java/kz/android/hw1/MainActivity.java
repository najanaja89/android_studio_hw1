package kz.android.hw1;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        View view = this.getWindow().getDecorView();

        final TextView tv = findViewById(R.id.textView);

        final Button button = findViewById(R.id.button);
        button.setBackgroundColor(Color.RED);
        button.setOnClickListener(v -> {
            tv.setBackgroundColor(Color.RED);
            view.setBackgroundColor(Color.YELLOW);
        });

        final Button button2 = findViewById(R.id.button2);
        button2.setBackgroundColor(Color.GREEN);
        button2.setOnClickListener(v -> {
            tv.setBackgroundColor(Color.GREEN);
            view.setBackgroundColor(Color.BLACK);
        });

        final Button button3 = findViewById(R.id.button3);
        button3.setBackgroundColor(Color.BLUE);
        button3.setOnClickListener(v -> {
            tv.setBackgroundColor(Color.BLUE);
            view.setBackgroundColor(Color.MAGENTA);
        });

    }
}